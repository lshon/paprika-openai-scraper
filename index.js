const express = require("express");
const yelp = require("yelp-fusion");
const { OpenAI, Configuration } = require("openai");
const {
    getReviews,
    getFoodInsights,
    scrapeYelpData,
    scrapeYelpMenu,
} = require("./controllers/yelpController");
const axios = require("axios");
const cheerio = require("cheerio");
const { analyzeMenu } = require("./controllers/openAIController");
require("dotenv").config();

const app = express();
const port = 3000;

// OpenAI configuration
const openai = new OpenAI();

// Yelp API configuration
const yelpApiKey = process.env.YELP_API_KEY;
const yelpClient = yelp.client(yelpApiKey);

// SEARCH CRITERIA
const searchRequest = {
    location: "Los Angeles", // Change this to any location you prefer
    categories: "restaurants",
    term: "Uovo", // Look for specific types of food
    limit: 1,
};

// Normalize website URL to include "www."
function normalizeWebsiteUrl(url) {
    if (!url.startsWith("http://") && !url.startsWith("https://")) {
        if (!url.startsWith("www.")) {
            url = "www." + url;
        }
        url = "http://" + url;
    }
    return url;
}

//Get Restaurant Details
async function getRestaurantDetails(searchRequest) {
    try {
        const response = await yelpClient.search(searchRequest);
        const businesses = response.jsonBody.businesses;
        // console.log(businesses);

        const restaurants = await Promise.all(
            businesses.map(async (business) => {
                const restaurantInfo = {
                    name: business.name,
                    phone: business.phone,
                    id: business.id,
                    alias: business.alias,
                    yelpUrl: business.url,
                };

                const business_id = business.id;
                const url = business.url;

                // GET REVIEWS
                // try {
                //     const reviews = await getReviews(business_id); // Await the async call
                //     restaurantInfo.reviews = reviews;
                // } catch (error) {
                //     console.error(
                //         "Error fetching reviews:",
                //         error.message,
                //         error
                //     );
                //     restaurantInfo.reviews = null;
                // }

                // GET FOOD INSIGHTS
                // try {
                //     const insights = await getFoodInsights(business_id); // Await the async call
                //     restaurantInfo.insights = insights;
                // } catch (error) {
                //     console.error(
                //         "Error fetching insights:",
                //         error.message,
                //         error
                //     );
                //     restaurantInfo.insights = null;
                // }

                //GET OFFICIAL WEBSITE
                try {
                    const data = await scrapeYelpData(url); // Await the async call
                    const website = normalizeWebsiteUrl(data[0]);
                    restaurantInfo.website = website;
                } catch (error) {
                    console.error(
                        "Error scraping website data:",
                        error.message
                    );
                    restaurantInfo.website = null;
                }

                //GET OFFICIAL MENU
                try {
                    const data = await scrapeYelpMenu(url); // Await the async call
                    const menu = normalizeWebsiteUrl(data[0]);
                    restaurantInfo.menu = menu;
                } catch (error) {
                    console.error("Error scraping menu data:", error.message);
                    restaurantInfo.menu = null;
                }
                // Analyze the website for dietary information
                if (restaurantInfo.menu) {
                    const dietaryInfo = await analyzeMenu(restaurantInfo.menu);
                    restaurantInfo.dietaryInfo = dietaryInfo;
                }

                return restaurantInfo;
            })
        );
        return restaurants;
    } catch (error) {
        console.error("Error fetching restaurant details:", error.message);
        return [];
    }
}

async function run() {
    const yelpResponse = await getRestaurantDetails(searchRequest);

    console.log("Restaurant Info", yelpResponse);
}

run();

app.listen(port, () => {
    console.log(`Server running at http://localhost:${port}`);
});
