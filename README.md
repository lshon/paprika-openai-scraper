# Paprika Backend OpenAI Script

-   Leo Shon

## Design

-   [OpenAi Functions](controllers/openAIController.js)
-   [YelpApi Functions](controllers/yelpController.js)
-   [Main Script](index.js)

## Project Initialization

Follow these steps to fully deploy this application on your local machine:

1. Clone the repository into your local machine
2. CD into the new projects directory
3. Add your `YELP_API_KEY` to a .env file
    1. make sure the .env file is at the top level of your directory.
4. Setup your OpenAI API Key
    1. follow instructions on the OpenAI documentation: https://platform.openai.com/docs/quickstart?context=node
5. Run `npm install`
6. Edit search criteria `const searchRequest` in index.js to your liking.
    1. note depending on what level your subscription is on YelpFusion and openAI, some restaurants may not return relevent data. It also depends if the restaurant has a available menu to view on their yelp business page.
7. Run `node index.js`
