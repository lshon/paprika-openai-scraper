// const express = require("express");
const axios = require("axios");
const cheerio = require("cheerio");
// const yelp = require("yelp-fusion");

const yelpApiKey = process.env.YELP_API_KEY;
async function getReviews(business_id) {
    //get food reviews

    const options = {
        method: "GET",
        url: `https://api.yelp.com/v3/businesses/${business_id}/reviews`,
        headers: {
            accept: "application/json",
            Authorization: `Bearer ${yelpApiKey}`,
        },
    };

    axios
        .request(options)
        .then(function (response) {
            // console.log("RESPONSE", response.data);
            return response.data;
        })
        .catch(function (error) {
            console.error(error);
        });
}

async function getFoodInsights(business_id) {
    // get food and drink insights

    console.log(business_id);
    const options = {
        method: "GET",
        url: `https://api.yelp.com/v3/businesses/${business_id}/insights/food_and_drinks`,
        headers: {
            accept: "application/json",
            Authorization: `Bearer ${yelpApiKey}`,
        },
    };

    axios
        .request(options)
        .then(function (insightResponse) {
            // console.log(insightResponse)
            return insightResponse.data;
        })
        .catch(function (error) {
            console.error(error);
        });
}

async function scrapeYelpMenu(pageUrl) {
    try {
        // Fetch the HTML content of the page
        const { data } = await axios.get(pageUrl);
        // Load the HTML into cheerio
        const $ = cheerio.load(data);

        // Extract the href attribute of the desired anchor tags
        const menuURLs = [];

        $("a").each((i, element) => {
            const text = $(element).text().trim();
            if (text.includes("View full menu")) {
                let href = $(element).attr("href");
                if (href) {
                    // If the URL is a Yelp redirect link, parse the actual URL
                    if (href.includes("/biz_redir")) {
                        const parsedUrl = new URLSearchParams(
                            href.split("?")[1]
                        );
                        href = parsedUrl.get("url");
                    }
                    // If the URL is relative, construct the full URL
                    const menuUrl = href.startsWith("http")
                        ? href
                        : `https://www.yelp.com${href}`;
                    menuURLs.push(menuUrl);
                }
            }
        });

        // Log if no menu URLs found
        if (menuURLs.length === 0) {
            console.warn("No menu URLs found on the page.");
        }

        // Return the scraped website URLs
        return menuURLs;
    } catch (error) {
        console.error("Error fetching or parsing data:", error);
        return null;
    }
}

async function scrapeYelpData(pageUrl) {
    try {
        // Fetch the HTML content of the page
        const { data } = await axios.get(pageUrl);
        // Load the HTML into cheerio
        const $ = cheerio.load(data);

        // Extract the href attribute of the desired anchor tags
        const websiteURLs = [];

        $('a[href^="/biz_redir?url="]').each((index, element) => {
            const href = $(element).attr("href");
            const parsedUrl = new URLSearchParams(href.split("?")[1]);
            const websiteUrl = parsedUrl.get("url");
            if (websiteUrl) {
                // Parse and extract the domain name
                const domain = new URL(websiteUrl).hostname;
                websiteURLs.push(domain);
            }
        });

        // Return the scraped website URLs
        return websiteURLs;
    } catch (error) {
        console.error("Error fetching or parsing data:", error);
        return null;
    }
}

module.exports = {
    getFoodInsights,
    getReviews,
    scrapeYelpData,
    scrapeYelpMenu,
};
