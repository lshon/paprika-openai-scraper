const { OpenAI, Configuration } = require("openai");
const axios = require("axios");
const cheerio = require("cheerio");

// OpenAI configuration
const openai = new OpenAI();

// Function to analyze dietary offerings
async function analyzeMenu(url) {
    try {
        const response = await axios.get(url);
        const html = response.data;
        const $ = cheerio.load(html);
        const textContent = $("body").text();
        // console.log("textcontent", textContent);

        const prompt = `Extract dietary information from the following text:\n\n${textContent}\n\n. Make sure to look for words such as 'vegan', 'allergy', 'allergen', 'ingredients, 'nuts' and keep an eye out for special ingredients that could be a potential allergens. Make sure to return any dishes that contain allergens such as nuts, cheese, meat etc. `;

        const openAIResponse = await openai.chat.completions.create({
            model: "gpt-3.5-turbo-1106",
            messages: [{ role: "user", content: prompt }],
            max_tokens: 500,
        });

        return openAIResponse.choices[0].message.content;
    } catch (error) {
        console.error("Error scraping and analyzing website:", error.message);
        return null;
    }
}

module.exports = {
    analyzeMenu,
};
